/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Francisco
 */
public class ProdutoTableModel extends AbstractTableModel {

    private List<Produto> dados = new ArrayList<>();
    private String[] colunas = {"Descrição", "Qauntidade", "Valor"};

    @Override
    public String getColumnName(int coluna) {
        //remonear as colunas,com nome corretamente
        return colunas[coluna];
    }
    

    @Override
    public int getRowCount() {
        //quantidade de linhas
        return dados.size();
    }

    @Override
    public int getColumnCount() {
        //quantidade de colunas
        return colunas.length;
    }

    @Override
    public Object getValueAt(int linha, int coluna) {

        switch (coluna) {
            case 0:
                return dados.get(linha).getDescricao();
            case 1:
                return dados.get(linha).getQtd();
            case 2:
                return dados.get(linha).getValor();

        }
        return "";
    }

    public void addRow(Produto produto){
        this.dados.add(produto);
        
        //verificar se houver mudanca e atualizar a tabela
        this.fireTableDataChanged();
    }
}
